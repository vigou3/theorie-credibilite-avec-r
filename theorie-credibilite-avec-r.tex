%%% Copyright (C) 2018-2025 Vincent Goulet
%%%
%%% Ce fichier et tous les fichiers .tex ou .Rnw dont la racine est
%%% mentionnée dans les commandes \include ci-dessous font partie du
%%% projet «Théorie de la crédibilité avec R»
%%% https://gitlab.com/vigou3/theorie-credibilite-avec-r
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[letterpaper,11pt,x11names,english,french]{memoir}
  \usepackage{natbib,url}
  \usepackage{babel}
  \usepackage[autolanguage,np]{numprint}
  \usepackage{amsmath,amsthm}
  \usepackage[noae]{Sweave}
  \usepackage{graphicx}
  \usepackage{currfile}                % pour noms fichiers de script
  \usepackage{pict2e}                  % graphique urne d'urne
  \usepackage{framed}                  % env. snugshade*, oframed
  \usepackage[absolute]{textpos}       % éléments des pages de titre
  \usepackage[shortlabels]{enumitem}   % configuration listes
  \usepackage{relsize}                 % \smaller et al.
  \usepackage{manfnt}                  % \mantriangleright (puce)
  \usepackage{metalogo}                % \XeLaTeX logo
  \usepackage{fontawesome5}            % icônes \fa*
  \usepackage{awesomebox}              % boites info, important, etc.
  \usepackage{answers}                 % exercices et solutions
  \usepackage{listings}                % code informatique

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{Théorie de la crédibilité avec R}
  \author{Vincent Goulet}
  \renewcommand{\year}{2025}
  \renewcommand{\month}{01}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/theorie-credibilite-avec-r}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Polices de caractères
  \usepackage{fontspec}
  \usepackage{unicode-math}
  \defaultfontfeatures
  {
    Scale = 0.92
  }
  \setmainfont{Lucida Bright OT}
  [
    Ligatures = TeX,
    Numbers = OldStyle
  ]
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \setsansfont{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 0.98,
    Numbers = OldStyle
  ]
  \newfontfamily\fullcaps{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    Scale = 0.98,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Couleurs
  \usepackage{xcolor}
  \definecolor{comments}{rgb}{0.5,0.55,0.6} % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}        % liens internes
  \definecolor{url}{rgb}{0.6,0,0}           % liens externes
  \definecolor{citation}{rgb}{0,0.5,0}      % citations
  \definecolor{codebg}{named}{LightYellow1} % fond code R
  \definecolor{darkblue}{rgb}{0,0,0.7}      % pour théorème 4.1
  \colorlet{darkred}{comments}              % idem
  \colorlet{darkgreen}{citation}            % idem
  \definecolor{lineno}{named}{gray}         % numéros de lignes
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % rouge bandeau UL
  \definecolor{or}{rgb}{1,0.8,0}            % or bandeau UL

  %% Hyperliens
  \usepackage[backref=page]{hyperref}
  \hypersetup{%
    pdfauthor = \theauthor,
    pdftitle = \thetitle,
    colorlinks = true,
    linktocpage = true,
    urlcolor = {url},
    linkcolor = {link},
    citecolor = {citation},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit}}
  \renewcommand*{\backrefalt}[4]{%
    \ifcase #1 %
      Aucune citation.%
    \or
      Cité à la page #2.%
    \else
      Cité aux pages #2.%
    \fi
  }
  \setlength{\XeTeXLinkMargin}{1pt}

  %% Affichage de la table des matières du PDF
  \usepackage{bookmark}
  \bookmarksetup{%
    open = true,
    depth = 3,
    numbered = true}

  %% Étiquettes de \autoref (redéfinitions compatibles avec babel).
  %% Attention! Les % à la fin des lignes sont importants sinon des
  %% blancs apparaissent dès que la commande \selectlanguage est
  %% utilisée... comme dans la bibliographie, par exemple.
  \addto\extrasfrench{%
    \def\appendixautorefname{annexe}%
    \def\definitionautorefname{défi\-ni\-tion}%
    \def\exempleautorefname{exemple}%
    \def\figureautorefname{figure}%
    \def\exerciceautorefname{exer\-cice}%
    \def\subfigureautorefname{figure}%
    \def\subsectionautorefname{section}%
    \def\subtableautorefname{tableau}%
    \def\tableautorefname{tableau}%
    \def\thmautorefname{théorème}%
  }

  %% Table des matières et al. (inspiré de classicthesis.sty)
  \renewcommand{\cftchapterleader}{\hspace{1.5em}}
  \renewcommand{\cftchapterafterpnum}{\cftparfillskip}
  \renewcommand{\cftsectionleader}{\hspace{1.5em}}
  \renewcommand{\cftsectionafterpnum}{\cftparfillskip}
  \renewcommand{\cfttableleader}{\hspace{1.5em}}
  \renewcommand{\cfttableafterpnum}{\cftparfillskip}
  \renewcommand{\cftfigureleader}{\hspace{1.5em}}
  \renewcommand{\cftfigureafterpnum}{\cftparfillskip}
  \setlength{\cftbeforechapterskip}{1.0em plus 0.5em minus 0.5em}
  \setlength{\cftfigurenumwidth}{3.2em}
  \setpnumwidth{2.55em}         % plus de la place pour les folios
  \setrmarg{3.55em}             % idem

  %% Création d'une «liste des vidéos» similaires à la
  %% liste des tableaux et à la liste des figures.
  \newcommand{\listvideosname}{Liste des vidéos}
  \newlistof{listofvideos}{lov}{\listvideosname}
  \renewcommand{\afterlovtitle}{%
    \afterchaptertitle{\normalfont\normalsize Le numéro indiqué à
      gauche est celui de la section dans laquelle se trouve le bloc
      signalétique.\label{listofvideos}}\vspace{10pt}}

  %% Marges, entêtes et pieds de page (dimensions du manuel de memoir)
  \settypeblocksize{8in}{33pc}{*} % hauteur augmentée de 0.25in
  \setulmargins{4cm}{*}{*}
  \setlrmargins{1.25in}{*}{*}
  \setmarginnotes{17pt}{51pt}{\onelineskip}
  \setheadfoot{\onelineskip}{2\onelineskip}
  \setheaderspaces{*}{2\onelineskip}{*}
  \checkandfixthelayout

  %% Titres des chapitres
  \chapterstyle{hangnum}
  \renewcommand{\chaptitlefont}{\normalfont\Huge\sffamily\bfseries\raggedright}

  %% Titres des sections et sous-sections
  \setsecheadstyle{\normalfont\Large\sffamily\bfseries\raggedright}
  \setsubsecheadstyle{\normalfont\large\sffamily\bfseries\raggedright}
  \maxsecnumdepth{subsection}
  \setsecnumdepth{subsection}

  %% Listes. Paramétrage avec enumitem.
  \setlist[enumerate]{leftmargin=*,align=left}
  \setlist[enumerate,2]{label=\alph*),labelsep=*,leftmargin=1.5em}
  \setlist[enumerate,3]{label=\roman*),labelsep=*,leftmargin=1.5em,align=right}
  \setlist[itemize]{leftmargin=*,align=left}

  %% Options de babel
  \frenchbsetup{%
    StandardItemizeEnv=true,%
    SmallCapsFigTabCaptions=true,
    ThinSpaceInFrenchNumbers=true,
    ItemLabeli=\mantriangleright,
    ItemLabelii=\textendash,
    og=«, fg=»}
  \babeltags{en = english}
  \renewcommand*{\frenchtablename}{Tab.}
  \renewcommand*{\frenchfigurename}{Fig.}
  \renewcommand*{\frenchlistfigurename}{Liste des figures}

  %%% =========================
  %%%  Sections de code source
  %%% =========================

  %% Syntaxe de R
  \lstloadlanguages{R}

  %% Mise en forme du code source.
  %%
  %% Les numéros de lignes sont des hyperliens vers le point dans le
  %% document où l'on y fait référence dans une boite \gotorbox (voir
  %% plus loin).
  %%
  %% Pour y parvenir, j'utilise deux étiquettes pour une ligne: une
  %% basée sur un "nom" utilisé dans la rédaction, et une autre
  %% générée automatiquement à partir du numéro de chapitre et du
  %% numéro de ligne.
  %%
  %% Solution basée sur https://tex.stackexchange.com/q/191771
  \lstset{%
    language=R,
    extendedchars=true,
    basicstyle=\small\ttfamily\NoAutoSpacing,
    commentstyle=\color{comments}\slshape,
    keywordstyle=\mdseries,
    showstringspaces=false,
    numbers=left,
    numberstyle={%
      \color{lineno}\tiny\ttfamily%
      \ifnum\value{lstnumber}=\getrefnumber{code:\thechapter:\thelstnumber}%
        \renewcommand*\thelstnumber{\hyperlink{goto:\thechapter:\the\value{lstnumber}}{\bfseries\arabic{lstnumber}}}%
      \fi},
    firstnumber=\scriptfirstline,
    escapechar=`}

  %% Commandes pour créer les références et liens vers des numéros de
  %% lignes.
  \makeatletter
  \newcommand\labelline[1]{%
    \def\@currentlabel{\thelstnumber}%
    \label{lst:#1}\label{code:\thechapter:\the\value{lstnumber}}}
  \makeatother
  \newcommand{\reflines}[1]{%
    \hypertarget{goto:\thechapter:\getrefnumber{lst:#1}}{\ref{lst:#1}}--%
    \hypertarget{goto:\thechapter:\getrefnumber{lst:#1:fin}}{\ref{lst:#1:fin}}}

  %% L'entête des fichiers de script n'est pas affiché dans le
  %% document.
  \def\scriptfirstline{10}      % nombre magique!

  %%% =========================
  %%%  Nouveaux environnements
  %%% =========================

  %% Environnements d'exemples et al.
  \theoremstyle{plain}
  \newtheorem{thm}{Théorème}[chapter]
  \newtheorem{corollaire}{Corollaire}[chapter]

  \theoremstyle{definition}
  \newtheorem{definition}{Définition}[chapter]
  \newtheorem{exemple}{Exemple}[chapter]

  %% Redéfinition de l'environnement titled-frame de framed.sty avec
  %% deux modifications: épaisseur des filets réduite de 2pt à 1pt;
  %% "(suite)" plutôt que "(cont)" dans la barre de titre
  %% lorsque l'encadré se poursuit après un saut de page.
  \renewenvironment{titled-frame}[1]{%
    \def\FrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame{\textbf{#1}}}%
    \def\FirstFrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame[$\blacktriangleright$]{\textbf{#1}}}%
    \def\MidFrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame[$\blacktriangleright$]{\textbf{#1\ (suite)}}}%
    \def\LastFrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame{\textbf{#1\ (suite)}}}%
    \MakeFramed{\advance\hsize-16pt \FrameRestore}}%
  {\endMakeFramed}

  %% Liste d'objectifs au début des chapitres dans un encadré
  %% basé sur titled-frame, ci-dessus.
  \newenvironment{objectifs}{%
    \colorlet{TFFrameColor}{black}%
    \colorlet{TFTitleColor}{white}%
    \begin{titled-frame}{\rule[-7pt]{0pt}{20pt}\sffamily Objectifs du chapitre}
      \setlength{\parindent}{0pt}
      \small\sffamily
      \begin{itemize}[nosep]}%
      {\end{itemize}\end{titled-frame}}

  %% Environnements de Sweave. Les environnements Sinput et Soutput
  %% utilisent Verbatim (de fancyvrb). On les réinitialise pour
  %% enlever la configuration par défaut de Sweave, puis on réduit
  %% l'écart entre les blocs Sinput et Soutput.
  \DefineVerbatimEnvironment{Sinput}{Verbatim}{}
  \DefineVerbatimEnvironment{Soutput}{Verbatim}{}
  \fvset{listparameters={\setlength{\topsep}{0pt}}}

  %% L'environnement Schunk est complètement redéfini en un hybride
  %% des environnements snugshade* et leftbar de framed.sty.
  \makeatletter
  \renewenvironment{Schunk}{%
    \setlength{\topsep}{1pt}
    \def\FrameCommand##1{\hskip\@totalleftmargin
       \vrule width 2pt\colorbox{codebg}{\hspace{3pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %% Exercices et réponses
  \Newassociation{sol}{solution}{solutions}
  \Newassociation{rep}{reponse}{reponses}
  \newcounter{exercice}[chapter]
  \renewcommand{\theexercice}{\thechapter.\arabic{exercice}}
  \newenvironment{exercice}[1][]{%
    \begin{list}{}{%
        \refstepcounter{exercice}
        \ifthenelse{\equal{#1}{nosol}}{%
          \renewcommand{\makelabel}{\bfseries\theexercice}}{%
          \hypertarget{ex:\theexercice}{}
          \Writetofile{solutions}{\protect\hypertarget{sol:\theexercice}{}}
          \renewcommand{\makelabel}{%
            \bfseries\protect\hyperlink{sol:\theexercice}{\theexercice}}}
        \settowidth{\labelwidth}{\bfseries\theexercice}
        \setlength{\leftmargin}{\labelwidth}
        \addtolength{\leftmargin}{\labelsep}
        \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
        \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
      \item}%
      {\end{list}}
  \renewenvironment{solution}[1]{%
    \begin{list}{}{%
        \renewcommand{\makelabel}{%
          \bfseries\protect\hyperlink{ex:#1}{#1}}
        \settowidth{\labelwidth}{\bfseries #1}
        \setlength{\leftmargin}{\labelwidth}
        \addtolength{\leftmargin}{\labelsep}
        \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
        \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
      \item}
    {\end{list}}
  \renewenvironment{reponse}[1]{%
    \begin{enumerate}[label=\textbf{#1}]
    \item}%
    {\end{enumerate}}

  %% Corriger un bogue dans answers apparu dans TeX Live 2020.
  %% https://tex.stackexchange.com/a/537873
  \makeatletter
  \renewcommand{\Writetofile}[2]{%
    \@bsphack
    \Iffileundefined{#1}{}{%
      \Ifopen{#1}{%
        {%
          \begingroup
          % \let\protect\string %%% <----- removed
          \Ifanswerfiles{%
            \protected@iwrite{\@nameuse{#1@file}}{\afterassignment\let@protect@string}{#2}%
          }{}%
          \endgroup
        }%
      }{}%
    }%
    \@esphack
  }
  \def\let@protect@string{\let\protect\string}
  \makeatother

  %%% =====================
  %%%  Nouvelles commandes
  %%% =====================

  %% Noms de fonctions, code, etc.
  \newcommand{\code}[1]{\texttt{#1}}
  \newcommand{\pkg}[1]{\textbf{#1}}

  %% Hyperlien avec symbole de lien externe juste après; second
  %% argument peut être vide pour afficher l'url comme lien
  %% [https://tex.stackexchange.com/q/53068/24355 pour procédure de
  %% test du second paramètre vide]
  \newcommand{\link}[2]{%
    \def\param{#2}%
    \ifx\param\empty
      \href{#1}{\nolinkurl{#1}~\raisebox{-0.1ex}{\smaller\faExternalLink*}}%
    \else
      \href{#1}{#2~\raisebox{-0.1ex}{\smaller\faExternalLink*}}%
    \fi
  }

  %% Boite additionnelle (basée sur awesomebox.sty) pour liens vers
  %% des vidéos et ajout à la liste des vidéos.
  \newcounter{videos}[chapter]
  \newcommand{\videobox}[2]{%
    \refstepcounter{videos}
    \addcontentsline{lov}{figure}{\protect\numberline{\thesection}\protect#1}
    \awesomebox{\aweboxrulewidth}{\faYoutube}{url}{#2}}

  %% Boite additionnelle (basée sur awesomebox.sty) pour changements
  %% au fil de la lecture.
  \newcommand{\gotorbox}[1]{%
    \awesomebox{\aweboxrulewidth}{\faMapSigns}{black}{#1}}

  %% Boite pour le nom du fichier de script correspondant au début des
  %% sections d'exemples.
  \newcommand{\scriptfile}[1]{%
    \begingroup
    \noindent
    \mbox{%
      \makebox[3mm][l]{\raisebox{-0.5pt}{\small\faChevronCircleDown}}\;%
      \smaller[1] {\sffamily Fichier d'accompagnement} {\ttfamily #1}}
    \endgroup}

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% Lien vers GitLab dans la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Raccourcis usuels vg
  \newcommand{\pt}{{\scriptscriptstyle \Sigma}}
  \newcommand{\Esp}[1]{E\! \left[ #1 \right]}
  \newcommand{\esp}[1]{E [ #1 ]}
  \newcommand{\Var}[1]{\operatorname{Var}\! \left[ #1 \right]}
  \newcommand{\var}[1]{\operatorname{Var} [ #1 ]}
  \newcommand{\Cov}{\operatorname{Cov}}
  \newcommand{\CV}{\operatorname{CV}}
  \ifxetex
    \newcommand{\mat}[1]{\symbf{#1}}
  \else
    \newcommand{\mat}[1]{\mathbf{#1}}
  \fi
  \ifxetex
    \newcommand{\Z}{\symbb{Z}}
  \else
    \newcommand{\Z}{\mathbb{Z}}
  \fi

  %%% =======
  %%%  Varia
  %%% =======

  %%% Sous-figures
  \newsubfloat{figure}

  %%% Style de la bibliographie
  \bibliographystyle{francais}

  %% Permettre la coupure des url aux traits d'union
  %% https://tex.stackexchange.com/a/256190
  \def\UrlBreaks{\do\/\do-}

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière.
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}

  %% Longueurs et type de colonne pour les tableaux du chapitre 1.
  \newlength{\tablewidth}
  \setlength{\tablewidth}{\textwidth}
  \addtolength{\tablewidth}{-2\parindent}
  \newcolumntype{Y}{>{\centering\arraybackslash$}X<{$}}

  %% Aide pour la césure
  \hyphenation{%
    moin-dres
  }

%  \includeonly{couverture-avant}

\begin{document}

\frontmatter

\pagestyle{empty}

\input{couverture-avant}
\null\cleartoverso              % cf. section 2.2 textpos.pdf

\include{notices}

%% Citation de Hans Bühlmann (recto)
\cleartorecto
\thispagestyle{empty}
\begin{vplace}[0.45]
  \begin{quote}
    \emph{Actuaries are like Frenchmen. They translate everything into their
      language and then it sounds beautiful, only nobody understands it
      anymore.} %
    \sourceatright{Hans Bühlmann}
  \end{quote}
\end{vplace}

\clearpage

\pagestyle{companion}

\include{introduction}

\tableofcontents
\cleartorecto
\listoftables
\cleartorecto
\listoffigures
\cleartorecto
\listofvideos

%% Vignette xkcd.com
\cleartoverso
\thispagestyle{empty}
\begin{vplace}[0.45]
  \centering
  \begin{minipage}{0.9\linewidth}
    \setkeys{Gin}{width=\textwidth}
    \includegraphics{images/insurance.png} \\
    \footnotesize\sffamily%
    Tiré de \href{https://xkcd.com/1494/}{XKCD.com}
  \end{minipage}
\end{vplace}

\mainmatter

\include{contexte}
\include{stabilite}
\include{bayesienne}
\include{buhlmann}
\include{buhlmann-straub}

\appendix
\include{estimation-bayesienne}
\include{formules-credibilite-lineaire}
\include{distributions}
\include{solutions}

\backmatter

\bibliography{cred,actu,stat,r,vg}

\pagestyle{empty}

\cleartoverso
\input{colophon}

\cleartoverso
\input{couverture-arriere}

\end{document}

%%% Local Variables:
%%% TeX-master: t
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
