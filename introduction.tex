%%% Copyright (C) 2018-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Théorie de la crédibilité avec R»
%%% https://gitlab.com/vigou3/theorie-credibilite-avec-r
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}
\markboth{Introduction}{Introduction}

«Les mathématiques de l'hétérogénéité.» C'est parfois ainsi que l'on
décrit la théorie de la crédibilité, la pierre angulaire des
mathématiques de l'assurance IARD. Cet ouvrage offre un traitement
rigoureux et exhaustif des modèles de base de la crédibilité, soit la
crédibilité de stabilité (\texten{\emph{limited fluctuations}}), la
tarification basée sur l'expérience purement bayésienne et les modèles
classiques de Bühlmann et de Bühlmann--Straub.

Le paquetage \pkg{actuar} \citep{actuar} pour l'environnement
statistique R \citep{R} permet d'effectuer les calculs relatifs aux
modèles de crédibilité abordés dans l'ouvrage. Nous expliquons comment
utiliser la fonction \code{cm} du paquetage par le biais de code
informatique qui est distribué avec l'ouvrage et reproduit à la fin
des chapitres \ref*{chap:bayesienne}, \ref*{chap:buhlmann} et
\ref*{chap:buhlmann-straub}.

L'ouvrage intègre également le recueil d'exercices et de solutions de
\citet{Cossette:credibilite:2008}. La collection d'exercices est le
fruit de la mise en commun d'exercices colligés au fil du temps pour
des cours de théorie de la crédibilité à l'Université Laval et à
l'Université Concordia. Certains exercices ont été rédigés par les
Professeurs François Dufresne et Jacques Rioux, entre autres. Quelques
exercices proviennent également d'anciens examens de la Society of
Actuaries et de la Casualty Actuarial Society.

Le premier chapitre, tiré de mon mémoire de maitrise
\citep{Goulet:masters}, trace l'historique et l'évolution de la
théorie de la crédibilité, de ses origines jusqu'au début des années
1990. Ce chapitre ne comporte pas d'exercices.

L'ouvrage contient trois annexes.
L'\autoref{chap:estimation-bayesienne} offre un sommaire des
principaux principes et résultats en estimation bayésienne. l'annexe
\autoref{chap:formules} contient les principales formules de
crédibilité linéaire. Enfin, \autoref{chap:distributions} présente la
paramétrisation des lois de probabilité utilisée dans les exercices.

\section*{Utilisation de l'ouvrage}

L'étude de l'ouvrage implique des allers-retours entre le texte et le
code R à la fin des chapitres. Ce code informatique et les
commentaires qui l'accompagnent visent à enrichir vos apprentissages.
Assurez-vous donc de lire attentivement tant les commentaires que le
code, d'exécuter le code pas-à-pas et de bien comprendre ses effets.

Le code informatique est distribué avec l'ouvrage sous forme de
fichiers de script. De plus, à chaque fichier \code{.R} correspond un
fichier \code{.Rout} contenant les résultats de son évaluation non
interactive.

\section*{Fonctionnalités interactives}

En consultation électronique, ce document se trouve enrichi de
plusieurs fonctionnalités interactives.
\begin{itemize}
\item Intraliens du texte vers une ligne précise d'une section de code
informatique et, en sens inverse, du numéro de la ligne vers le point
de la référence dans le texte. Ces intraliens sont marqués par la
couleur \textcolor{link}{\rule{1.5em}{1.2ex}}.
\item Intraliens entre le numéro d'un exercice et sa solution, et vice
versa. Ces intraliens sont aussi marqués par la couleur
\textcolor{link}{\rule{1.5em}{1.2ex}}.
\item Intraliens entre les citations dans le texte et leur entrée dans
la bibliographie. Ces intraliens sont marqués par la couleur
\textcolor{citation}{\rule{1.5em}{1.2ex}}.
\item Hyperliens vers des ressources externes marqués par le symbole
{\smaller\faExternalLink*} et la couleur
\textcolor{url}{\rule{1.5em}{1.2ex}}.
\item Table des matières, liste des tableaux et liste des figures
permettant d'accéder rapidement à des ressources du document.
\end{itemize}

\section*{Blocs signalétiques}

Le document est parsemé de divers types de blocs signalétiques
inspirés de
\link{https://asciidoctor.org/docs/user-manual/\#admonition}{AsciiDoc}
qui visent à attirer votre attention sur une notion. Vous pourrez
rencontrer l'un ou l'autre des blocs suivants.

\tipbox{Astuce! Ces blocs contiennent un truc, une astuce, ou tout
  autre type d'information utile.}
\vspace{-\baselineskip}

\warningbox{Avertissement! Ces blocs mettent l'accent sur une notion
  ou fournissent une information importante pour la suite.}
\vspace{-\baselineskip}

\importantbox{Important! Ces blocs contiennent les remarques les plus
  importantes. Veillez à en tenir compte.}
\vspace{-\baselineskip}

%% Utilisation de la commande \awesomebox pour illustrer les blocs de
%% vidéos pour éviter de créer une entrée dans la liste des vidéos.
\awesomebox{\aweboxrulewidth}{\faYoutube}{url}{Ces blocs
  contiennent des liens vers des vidéos dans la %
  \link{https://www.youtube.com/channel/UCUzjz0RoyiU6oAI7AO_DCkg/videos}{chaine
    YouTube} liée à ce document. Les vidéos sont répertoriées dans la
  liste des vidéos.}
\vspace{-\baselineskip}

\gotorbox{Ces blocs vous invitent à interrompre la lecture du texte
  pour passer à l'étude du code R des sections d'exemples.}
\vspace{-\baselineskip}

\section*{Document libre}

Tout comme R et l'ensemble des outils présentés dans ce document, le
projet \emph{\thetitle} s'inscrit dans le mouvement de
l'\link{https://www.gnu.org/philosophy/free-sw.html}{informatique
  libre}. Vous pouvez accéder à l'ensemble du code source en format
{\LaTeX} en suivant le lien dans la page de copyright. Vous trouverez
dans le fichier \code{README.md} toutes les informations utiles pour
composer le document.

Votre contribution à l'amélioration du document est également la
bienvenue; consultez le fichier \code{CONTRIBUTING.md} fourni avec ce
document et voyez votre nom ajouté au fichier \code{COLLABORATEURS}.


%%% Local Variables:
%%% TeX-master: "theorie-credibilite-avec-r"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
